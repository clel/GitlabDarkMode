GitLab dark mode so our corneas don't hurt. To test, add it via your favorite CSS injector (such as [Stylus](https://add0n.com/stylus.html)).

# Project goals

* Minimum amount of CSS with maximum maintainability (I don't want it to become [this](https://gitlab.com/vednoc/dark-gitlab/-/blob/master/gitlab.user.styl))
* Use/tweak original color whenever possible. Don't force specific colors.
* A11y - ensure reasonable contrast is maintained
* Use functionality supported by at least [96% of browser base](https://caniuse.com/).
* Maintain commentary for non-uniform GitLab elements that should be merged whilst groking GitLab's CSS structure.
* Get integrated into GitHub :sparkles:

# Files
- [Core theme](https://gitlab.com/lorin/GitlabDarkMode/-/blob/master/darkmode.css), apply it against entire gitlab.com domain for maximum test foot print.
- [Illustration/icon preview CSS](https://gitlab.com/lorin/GitlabDarkMode/-/blob/master/illustration-microsite.css) specific to [GitLab SVG project microsite](https://gitlab-org.gitlab.io/gitlab-svgs/) (for @jeldergl and co.)